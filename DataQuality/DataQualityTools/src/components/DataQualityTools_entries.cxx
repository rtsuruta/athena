#include "DataQualityTools/DataQualityFatherMonTool.h"
#include "DataQualityTools/DQTDetSynchMonTool.h"
#include "DataQualityTools/DQTMuonIDTrackTool.h"
//#include "DataQualityTools/DQTNonCollBkg_ZDC.h"
#include "DataQualityTools/DQTBackgroundMon.h"
#include "DataQualityTools/DQTDataFlowMonTool.h"
#include "DataQualityTools/DQTGlobalWZFinderTool.h"
#include "DataQualityTools/DQTLumiMonTool.h"
#include "DataQualityTools/DQTDataFlowMonAlg.h"

DECLARE_COMPONENT( DataQualityFatherMonTool )
DECLARE_COMPONENT( DQTDetSynchMonTool )
DECLARE_COMPONENT( DQTMuonIDTrackTool )
//DECLARE_COMPONENT( DQTNonCollBkg_ZDC )
DECLARE_COMPONENT( DQTBackgroundMon )
DECLARE_COMPONENT( DQTDataFlowMonTool )
DECLARE_COMPONENT( DQTGlobalWZFinderTool )
DECLARE_COMPONENT( DQTLumiMonTool )
DECLARE_COMPONENT( DQTDataFlowMonAlg )

